package com.hurricanedevelopment.flyingfood;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class CommandExecutor {
		
	/**
	 * Executes the appropriate command or error message dependent on
	 * the inputs `sender`, `cmd`, `label`, and `args`
	 *
	 * @param  sender the entity or executor of the command
	 * @param  cmd    the command sent i.e. /<cmd>
	 * @param  label  alias of the sent command
	 * @param  args   a string array of the arguments i.e. /<cmd> <arg1>
	 * @return        a boolean of whether the command was successful
	 */
	public static boolean command(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (cmd.getName().toLowerCase().equals("fooddrops")) {
			if (args.length == 1) {
				//enables FlyingFood plugin
				if (args[0].equals("enable")) {
					if (FlyingFood.enabled) 
						sender.sendMessage(ChatColor.RED + "Food drops already enabled!");
					else {
						FlyingFood.enabled = true;
						sender.sendMessage(ChatColor.GREEN + "Food drops enabled.");
						FlyingFood.init();
					}
				}
				//disables FlyingFood plugin
				else if (args[0].equals("disable")) {
					if (!FlyingFood.enabled) 
						sender.sendMessage(ChatColor.RED + "Food drops already disabled!");
					else {
						FlyingFood.enabled = false;
						sender.sendMessage(ChatColor.GREEN + "Food drops disabled.");
						Bukkit.getServer().getScheduler().cancelTask(FlyingFood.task);
					}
				}
				//toggles FlyingFood plugin
				else if (args[0].equals("toggle")) {
					if (FlyingFood.enabled) {
						FlyingFood.enabled = false;
						sender.sendMessage(ChatColor.GREEN + "Food drops disabled.");
						Bukkit.getServer().getScheduler().cancelTask(FlyingFood.task);
					} else {
						FlyingFood.enabled = true;
						sender.sendMessage(ChatColor.GREEN + "Food drops enabled.");
						FlyingFood.init();
					}
				}
				else
				{
					sender.sendMessage(ChatColor.RED + "/fooddrops <enable:disable:toggle>");
					return true;
				}
			}
			else
			{
				sender.sendMessage(ChatColor.RED + "/fooddrops <enable:disable:toggle>");
				return true;
			}
		}
		return false;
	}

}
