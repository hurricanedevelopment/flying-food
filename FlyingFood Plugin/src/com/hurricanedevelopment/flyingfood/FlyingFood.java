package com.hurricanedevelopment.flyingfood;

import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class FlyingFood extends JavaPlugin {
	
	public static Logger log = Bukkit.getLogger();
	public static FlyingFood plugin;
	public static int task;
	public static boolean enabled;
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		plugin = this;
		enabled = true;
		//assign the listeners
		getServer().getPluginManager().registerEvents(new Listeners(),this);
		init();
	}
	
	@Override
	public void onDisable() {
		reloadConfig();
		saveConfig();
		enabled = false;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		return CommandExecutor.command(sender,cmd,label,args);
	}
	
	/**
	 * Initializes food drops around player locations
	 */
	public static void init() {
		
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        task = scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {
            @Override
            public void run() {
            	dropFood();
            }
        },0,plugin.getConfig().getInt("droptime") * 20 * 60);        
	}
	
	/**
	 * For each player, it calls drop item on their person with each food item, and
	 * tells them food has dropped (depending on config.yml)
	 */
	public static void dropFood() {
		Player [] onlinePlayers = Bukkit.getServer().getOnlinePlayers();
		for (Player a : onlinePlayers) {
			Set<String> drops = plugin.getConfig().getConfigurationSection("drops").getKeys(true);
			for (String b : drops) {
				dropItem(a, b);
			}
			if (plugin.getConfig().getBoolean("settings.message"))
				a.sendMessage(ChatColor.LIGHT_PURPLE + "Food dropped.");
		}
	}
	
	/**
	 * Changes locations depending on type of food,
	 * creates item stack, tags with 'Edible' lore,
	 * and drops it in the world
	 */
	public static void dropItem(Player player, String creator) {
		Location drop = player.getLocation();
		drop.setY(255);
		int num = plugin.getConfig().getInt("drops." + creator);
		ItemStack toDrop = new ItemStack(Material.COBBLESTONE, 1);
		switch (creator) {
			case "bread":
				toDrop = new ItemStack(Material.BREAD, num);
				break;
			case "carrot":
				toDrop = new ItemStack(Material.CARROT_ITEM, num);
				drop.setX(drop.getX() + 2);
				break;
			case "baked_potato":
				toDrop = new ItemStack(Material.BAKED_POTATO, num);
				drop.setX(drop.getX() - 2);
				break;
			case "pumpkin_pie":
				toDrop = new ItemStack(Material.PUMPKIN_PIE, num);
				drop.setZ(drop.getZ() + 2);
				break;
			case "cookie":
				toDrop = new ItemStack(Material.COOKIE, num);
				drop.setZ(drop.getZ() - 2);
				break;
			case "melon":
				toDrop = new ItemStack(Material.MELON, num);
				drop.setX(drop.getX() + 1);
				drop.setZ(drop.getZ() + 1);
				break;
			case "mushroom_soup":
				toDrop = new ItemStack(Material.MUSHROOM_SOUP, num);
				drop.setX(drop.getX() - 1);
				drop.setZ(drop.getZ() - 1);
				break;
			case "cooked_chicken":
				toDrop = new ItemStack(Material.COOKED_CHICKEN, num);
				drop.setX(drop.getX() + 1);
				drop.setZ(drop.getZ() - 1);
				break;
			case "steak":
				toDrop = new ItemStack(Material.COOKED_BEEF, num);
				drop.setX(drop.getX() - 1);
				drop.setZ(drop.getZ() + 1);
				break;
			case "cooked_fish":
				toDrop = new ItemStack(Material.COOKED_FISH, num);
				drop.setX(drop.getX() + 2);
				drop.setZ(drop.getZ() + 2);
				break;
			case "cooked_pork":
				toDrop = new ItemStack(Material.GRILLED_PORK, num);
				drop.setX(drop.getX() - 2);
				drop.setZ(drop.getZ() - 2);
				break;
			case "apple":
				toDrop = new ItemStack(Material.APPLE, num);
				drop.setX(drop.getX() + 2);
				drop.setZ(drop.getZ() - 2);
				break;
			case "rotten_flesh":
				toDrop = new ItemStack(Material.ROTTEN_FLESH, num);
				drop.setX(drop.getX() - 2);
				drop.setZ(drop.getZ() + 2);
			    break;
		}
		if (num > 0) {
			ArrayList<String> temp = new ArrayList<String>();
			temp.add("Edible");
			ItemMeta tempMeta = toDrop.getItemMeta();
			tempMeta.setLore(temp);
			toDrop.setItemMeta(tempMeta);
			player.getWorld().dropItem(drop, toDrop);
		}
	}
}
