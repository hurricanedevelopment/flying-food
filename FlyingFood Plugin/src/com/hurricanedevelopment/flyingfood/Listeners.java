package com.hurricanedevelopment.flyingfood;


import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Listeners implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void Eat(PlayerItemConsumeEvent e) {
		//test if plugin enabled
		if (FlyingFood.enabled) {
			//gets current item and player
			Player player = e.getPlayer();
			ItemStack item = e.getItem();
			ItemMeta itemMeta = item.getItemMeta();
			boolean okay = false;
			//checks if it has the correct lore ('Edible')
			if (itemMeta != null && itemMeta.hasLore()) {
				List<String> lore = itemMeta.getLore();
				okay = lore.contains("Edible");
			}
			//doesn't allow consumption of food w/o edible lore, and if its NOT a golden apple
			if (!okay && item.getType() != Material.GOLDEN_APPLE) {
				e.setItem(new ItemStack(Material.BREAD, 1));
				player.setSaturation(player.getSaturation() - 6);
				player.setFoodLevel(player.getFoodLevel()-5);
				if (item.getAmount() > 1) 
					player.getInventory().removeItem(new ItemStack(item.getType(), 1));
				else
					player.getInventory().removeItem(item);
				//i know it's deprecated, but Bukkit's still broken
				player.updateInventory();
			}
		}
	}
}
